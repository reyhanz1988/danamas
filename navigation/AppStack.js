import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LanguagesScreen from '../screens/LanguagesScreen';

const Stack = createStackNavigator();

const HomeNav = () => {
    return(
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen 
                name="Home"
                component={HomeScreen}
            />
        </Stack.Navigator>
    )
}
export {HomeNav}

const SettingsNav = () => {
    return(
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen 
                name="Settings"
                component={SettingsScreen}
            />
            <Stack.Screen
                name="Languages"
                component={LanguagesScreen}
            />
        </Stack.Navigator>
    )
}
export {SettingsNav}