import React, { useContext } from "react";
import { LangContext } from "../App";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HomeNav, SettingsNav } from "./AppStack";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/Navigation";

const Tab = createBottomTabNavigator();

const AppNav = () => {
    const { currentLang } = useContext(LangContext);
	return (
		<Tab.Navigator
			initialRouteName="HomeNav"
			screenOptions={{
				tabBarActiveTintColor: FIRST_COLOR,
				tabBarInactiveTintColor: "#666",
				headerShown: false,
				tabBarItemStyle: { backgroundColor: SECOND_COLOR, marginBottom:5 },
				tabBarStyle: [
					{ display: "flex", height: 60 },
					null,
				],
			}}>
			<Tab.Screen
				name="HomeNav"
				component={HomeNav}
				options={{
					tabBarLabel: languages[0][currentLang],
                    tabBarLabelStyle: {fontSize:12},
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons
							name="home"
							color={color}
							size={24}
						/>
					),
				}}
			/>
			<Tab.Screen
				name="SettingsNav"
				component={SettingsNav}
				options={{
					tabBarLabel: languages[1][currentLang],
                    tabBarLabelStyle: {fontSize:12},
					tabBarIcon: ({ color }) => (
						<MaterialCommunityIcons
							name="cog"
							color={color}
							size={24}
						/>
					),
				}}
			/>
		</Tab.Navigator>
	);
};
export default AppNav;
