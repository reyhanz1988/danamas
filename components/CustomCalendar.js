import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../App";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { Button, IconButton, List, Menu } from "react-native-paper";
import { Calendar } from "react-native-calendars";
import { FIRST_COLOR, SECOND_COLOR } from "@env";
import languages from "../screensTranslations/components/CustomCalendar";
import PropTypes from "prop-types";

const curDate = new Date();
const getToday = {
	initialDate:
		curDate.getFullYear() +
		"-" +
		("0" + (curDate.getMonth() + 1)).slice(-2) +
		"-" +
		("0" + curDate.getDate()).slice(-2),
	todayDate:
		curDate.getFullYear() +
		"-" +
		("0" + (curDate.getMonth() + 1)).slice(-2) +
		"-" +
		("0" + curDate.getDate()).slice(-2),
	currentYear: curDate.getFullYear(),
	currentMonth: ("0" + (curDate.getMonth() + 1)).slice(-2),
};

const CustomCalendar = ({
	monthsArr,
	propsGetHolidays,
	propsGetHolidaysRes,
}) => {
	const { currentLang } = useContext(LangContext);
	const [getDataError, setGetDataError] = useState([]);
	const [markingType, setMarkingType] = useState("custom");
	const [initialDate, setInitialDate] = useState(getToday.initialDate);
	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");
	const [currentMonth, setCurrentMonth] = useState(getToday.currentMonth);
	const [currentMonthText, setCurrentMonthText] = useState("01");
	const [monthVisibility, setMonthVisibility] = useState(false);
	const [currentYear, setCurrentYear] = useState(getToday.currentYear);
	const [yearVisibility, setYearVisibility] = useState(false);
	const [weekends, setWeekends] = useState([]);
	const [getHolidays, setGetHolidays] = useState([]);
	const [markedDates, setMarkedDates] = useState({});

	const vars = {};
	vars.currentLang = currentLang;
	vars.currentMonth = currentMonth;
	vars.currentYear = currentYear;

	const getData = () => {
		propsGetHolidays(vars);
	};

	useEffect(() => {
		getData();
	}, []);
	//PROPS UPDATE
	useEffect(() => {
		if (propsGetHolidaysRes == "error") {
			setGetDataError(propsGetHolidaysRes);
		} else {
			if (propsGetHolidaysRes.status_msg) {
				setGetDataError(propsGetHolidaysRes.status_msg);
			} else {
				setGetHolidays(propsGetHolidaysRes);
				setGetDataError("");
			}
		}
	}, [propsGetHolidaysRes]);
	useEffect(() => {
		setCurrentMonthText(monthsArr[parseInt(currentMonth) - 1]);
	}, [monthsArr]);
	//UPDATE YEAR, MONTH, AND DATE
	const onDayPress = (selected) => {
		setMarkingType("period");
		if (!startDate) {
			setStartDate(selected.dateString);
		} else {
			if (!endDate) {
				let start = new Date(startDate).getTime();
				let end = new Date(selected.dateString).getTime();
				if (end > start) {
					setEndDate(selected.dateString);
				} else {
					setStartDate(selected.dateString);
				}
			} else {
				reset();
			}
		}
	};
	const onMonthChange = (month, monthText) => {
		setCurrentMonth(month);
		setCurrentMonthText(monthsArr[monthText]);
		setInitialDate(currentYear + "-" + month + "-01");
		toggleMonthVisibility();
	};
	const prevMonth = (prev) => {
		if (prev == 0) {
			setCurrentMonth("12");
			setCurrentMonthText(monthsArr[11]);
			let updateCurrentYear = currentYear - 1;
			setCurrentYear(updateCurrentYear);
			setInitialDate(updateCurrentYear + "-01-01");
		} else {
			setCurrentMonth(("0" + prev).slice(-2));
			setCurrentMonthText(monthsArr[parseInt(prev) - 1]);
			setInitialDate(currentYear + "-" + ("0" + prev).slice(-2) + "-01");
		}
	};
	const nextMonth = (next) => {
		if (next == 13) {
			setCurrentMonth("01");
			setCurrentMonthText(monthsArr[0]);
			let updateCurrentYear = currentYear + 1;
			setCurrentYear(updateCurrentYear);
			setInitialDate(updateCurrentYear + "-01-01");
		} else {
			setCurrentMonth(("0" + next).slice(-2));
			setCurrentMonthText(monthsArr[parseInt(next) - 1]);
			setInitialDate(currentYear + "-" + ("0" + next).slice(-2) + "-01");
		}
	};
	const onYearChange = (year) => {
		setCurrentYear(year);
		setInitialDate(year + "-" + currentMonth + "-01");
		toggleYearVisibility();
	};
	const reset = () => {
		setMarkingType("custom");
		setStartDate("");
		setEndDate("");
	};
	//GET WEEKENDS
	const getWeekends = () => {
		getData();
		let curDate = new Date(initialDate);
		let totalDays = new Date(
			curDate.getFullYear(),
			curDate.getMonth(),
			0
		).getDate();
		let weekendDates = new Array();
		for (let i = 1; i <= totalDays; i++) {
			let newDate = new Date(
				curDate.getFullYear(),
				curDate.getMonth(),
				i
			);
			if (newDate.getDay() == 0 || newDate.getDay() == 6) {
				weekendDates.push({
					date:
						curDate.getFullYear() +
						"-" +
						("0" + (curDate.getMonth() + 1)).slice(-2) +
						"-" +
						("0" + i).slice(-2),
				});
			}
		}
		setWeekends(weekendDates);
	};
	useEffect(() => {
		getWeekends();
	}, [initialDate]);
	//GET MARKED
	const getDaysArray = (start, end) => {
		Date.prototype.addDays = function (days) {
			let date = new Date(this.valueOf());
			date.setDate(date.getDate() + days);
			return date;
		};
		let dateArray = new Array();
		let currentDate = start;
		while (currentDate <= end) {
			dateArray.push(new Date(currentDate));
			currentDate = currentDate.addDays(1);
		}
		let dateString = [];
		for (let i = 0; i < dateArray.length; i++) {
			dateString.push(
				dateArray[i].getFullYear() +
					"-" +
					("0" + (dateArray[i].getMonth() + 1)).slice(-2) +
					"-" +
					("0" + dateArray[i].getDate()).slice(-2)
			);
		}
		return dateString;
	};
	const getMarked = () => {
		if (markingType == "custom") {
			//mark weekends
			let markedWeekends = {};
			let weekendStyle = {};
			weekendStyle = {
				customStyles: {
					text: {
						color: "blue",
					},
				},
			};
			for (let i = 0; i < weekends.length; i++) {
				markedWeekends[weekends[i].date] = weekendStyle;
			}
			//mark todayDate
			let markedTodayDate = {};
			markedTodayDate[getToday.todayDate] = {
				selected: true,
				selectedColor: "green",
			};
			//mark publicholidays
			let markedPublicHolidays = {};
			let holidayStyle = {};
			holidayStyle = {
				customStyles: {
					text: {
						color: "red",
					},
				},
			};
			if (getHolidays.length > 0) {
				let holidays = [];
				for (let i = 0; i < getHolidays.length; i++) {
					holidays[i] = getHolidays[i].holiday_date.split("-");
					markedPublicHolidays[
						holidays[i][0] +
							"-" +
							holidays[i][1] +
							"-" +
							("0" + holidays[i][2]).slice(-2)
					] = holidayStyle;
				}
			}
			setMarkedDates({
				...markedWeekends,
				...markedTodayDate,
				...markedPublicHolidays,
			});
		} else {
			let markedStartDate = {};
			let markedRange = {};
			let markedEndDate = {};
			if (startDate && endDate) {
				markedStartDate[startDate] = {
					startingDay: true,
					color: "#50cebb",
					textColor: "white",
				};
				markedEndDate[endDate] = {
					endingDay: true,
					color: "#50cebb",
					textColor: "white",
				};
				let datesArr = getDaysArray(
					new Date(startDate),
					new Date(endDate)
				);
				if (datesArr.length > 0) {
					for (let i = 0; i < datesArr.length; i++) {
						markedRange[datesArr[i]] = {
							color: "#70d7c7",
							textColor: "white",
						};
					}
				}
			} else {
				markedStartDate[startDate] = {
					startingDay: true,
					color: "#50cebb",
					textColor: "white",
				};
			}
			setMarkedDates({
				...markedRange,
				...markedStartDate,
				...markedEndDate,
			});
		}
	};
	useEffect(() => {
		if (weekends.length > 0 || startDate || endDate) getMarked();
	}, [weekends, getHolidays, startDate, endDate]);

	const toggleMonthVisibility = () => {
		setMonthVisibility(!monthVisibility);
	};

	const toggleYearVisibility = () => {
		setYearVisibility(!yearVisibility);
	};
	let monthsMenu = [];
	for (let i = 0; i < monthsArr.length; i++) {
		monthsMenu.push(
			<Menu.Item
				key={"month_" + i}
				onPress={() => onMonthChange(("0" + (i + 1)).slice(-2), i)}
				title={monthsArr[i]}
			/>
		);
	}
	let yearsMenu = [];
	for (let i = currentYear - 5; i <= currentYear + 5; i++) {
		yearsMenu.push(
			<Menu.Item
				key={"year_" + i}
				onPress={() => onYearChange(i)}
				title={i}
			/>
		);
	}
	let holidaysDisplay;
	let holidaysList = [];
	if (getHolidays.length > 0) {
		let holidaysDate = [];
		for (let i = 0; i < getHolidays.length; i++) {
			holidaysDate[i] = getHolidays[i].holiday_date.split("-");
			holidaysList.push(
				<List.Item
					key={"holidaysList_" + i}
					title={getHolidays[i].holiday_name}
					titleStyle={
						getHolidays[i].is_national_holiday == true
							? { color: "red" }
							: { color: "#666" }
					}
					left={() => (
						<Text
							style={[
								{ marginTop: 7, marginRight: 10 },
								getHolidays[i].is_national_holiday == true
									? { color: "red" }
									: { color: "#666" },
							]}>
							{holidaysDate[i][2]}
						</Text>
					)}
				/>
			);
		}
		holidaysDisplay = (
			<View style={styles.holidaysDisplay}>{holidaysList}</View>
		);
	}
	//RENDER
	if (getDataError != "") {
		getToday();
	} else {
		return (
			<ScrollView style={styles.CalendarWrapper}>
				<Calendar
					initialDate={initialDate}
					onDayPress={(selected) => onDayPress(selected)}
					hideArrows={true}
					renderHeader={() => {
						return (
							<View style={styles.CalendarHeader}>
								<View style={styles.CalendarFlexHeader}>
									<IconButton
										icon="chevron-double-left"
										color={FIRST_COLOR}
										onPress={() =>
											prevMonth(
												parseInt(currentMonth) - 1
											)
										}
									/>
								</View>
								<View style={styles.CalendarFlexHeader}>
									<Menu
										visible={monthVisibility}
										onDismiss={() =>
											toggleMonthVisibility()
										}
										anchor={
											<Button
												mode="contained"
												onPress={() =>
													toggleMonthVisibility()
												}
												labelStyle={{
													color: SECOND_COLOR,
												}}>
												{currentMonthText}
											</Button>
										}>
										{monthsMenu}
									</Menu>
								</View>
								<View style={styles.CalendarFlexHeader}>
									<Menu
										visible={yearVisibility}
										onDismiss={() => toggleYearVisibility()}
										anchor={
											<Button
												mode="contained"
												onPress={() =>
													toggleYearVisibility()
												}
												labelStyle={{
													color: SECOND_COLOR,
												}}>
												{currentYear}
											</Button>
										}>
										{yearsMenu}
									</Menu>
								</View>
								<View>
									<IconButton
										icon="chevron-double-right"
										color={FIRST_COLOR}
										onPress={() =>
											nextMonth(
												parseInt(currentMonth) + 1
											)
										}
									/>
								</View>
							</View>
						);
					}}
					markingType={markingType}
					markedDates={markedDates}
				/>
				<View style={styles.ResetButtonWrapper}>
					<Button
						mode="contained"
						onPress={() => reset()}
						labelStyle={{ color: SECOND_COLOR }}>
						{languages[0][currentLang]}
					</Button>
				</View>
				{holidaysDisplay}
			</ScrollView>
		);
	}
};

CustomCalendar.propTypes = {
	monthsArr: PropTypes.array,
	propsGetHolidays: PropTypes.array,
	propsGetHolidaysRes: PropTypes.array,
};

const styles = StyleSheet.create({
	CalendarWrapper: {
		padding: 5,
	},
	CalendarHeader: {
		borderBottomWidth: 1,
		borderBottomColor: "#666",
		flex: 1,
		flexDirection: "row",
		paddingTop: 10,
		paddingBottom: 10,
		justifyContent: "center",
		alignItems: "center",
	},
	CalendarFlexHeader: {
		marginRight: 10,
	},
	ResetButtonWrapper: {
		margin: 20,
	},
	holidaysDisplay: {
		padding: 20,
	},
});

export default CustomCalendar;
