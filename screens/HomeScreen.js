import React, { useContext, useEffect, useState } from "react";
import { LangContext } from "../App";
import { Dimensions, Image, StyleSheet, View } from "react-native";
import { Appbar } from "react-native-paper";
import CustomCalendar from "../components/CustomCalendar";
import logo from "../assets/images/logo.png";
import { connect } from "react-redux";
import * as productActions from "../actions/productActions";
import languages from "../screensTranslations/Home";
import { SECOND_COLOR } from "@env";
import PropTypes from "prop-types";

const HomeScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	const [monthsArr, setMonthsArr] = useState([
		languages[1][currentLang],
		languages[2][currentLang],
		languages[3][currentLang],
		languages[4][currentLang],
		languages[5][currentLang],
		languages[6][currentLang],
		languages[7][currentLang],
		languages[8][currentLang],
		languages[9][currentLang],
		languages[10][currentLang],
		languages[11][currentLang],
		languages[12][currentLang],
	]);
	useEffect(() => {
		setMonthsArr([
			languages[1][currentLang],
			languages[2][currentLang],
			languages[3][currentLang],
			languages[4][currentLang],
			languages[5][currentLang],
			languages[6][currentLang],
			languages[7][currentLang],
			languages[8][currentLang],
			languages[9][currentLang],
			languages[10][currentLang],
			languages[11][currentLang],
			languages[12][currentLang],
		]);
	}, [currentLang]);

	//RENDER
	return (
		<View style={styles.wrapper}>
			<Appbar.Header style={styles.AppHeader}>
				<Image source={logo} style={styles.Logo} />
			</Appbar.Header>
			<CustomCalendar
				monthsArr={monthsArr}
				propsGetHolidays={props.getHolidays}
				propsGetHolidaysRes={props.getHolidaysRes}
			/>
		</View>
	);
};

HomeScreen.propTypes = {
	getHolidays: PropTypes.func,
	getHolidaysRes: PropTypes.object,
};

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
	AppHeader: {
		backgroundColor: SECOND_COLOR,
	},
	Logo: {
		resizeMode: "contain",
		width: Dimensions.get("window").width,
		height: 40,
	},
});

function mapStateToProps(state) {
	return {
		getHolidaysRes: state.productReducer.getHolidaysRes,
	};
}
const mapDispatchToProps = {
	...productActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
