import React, { useContext } from "react";
import { LangContext } from "../App";
import {
	ScrollView,
	StyleSheet,
	View,
} from "react-native";
import { Appbar, List } from "react-native-paper";
import languages from "../screensTranslations/Settings";
import PropTypes from "prop-types";

const SettingsScreen = (props) => {
	const { currentLang } = useContext(LangContext);
	return (
		<View style={styles.wrapper}>
			<Appbar.Header>
				<Appbar.Content
					titleStyle={{ color: "#fff", textAlign: "center" }}
					title={languages[0][currentLang]}
				/>
			</Appbar.Header>
			<ScrollView style={{ marginLeft: 10, marginRight: 10 }}>
				<List.Item
					title={languages[1][currentLang]}
					left={(props) => (
						<List.Icon {...props} icon={"account-multiple-plus"} />
					)}
					right={(props) => (
						<List.Icon {...props} icon="chevron-right" />
					)}
					onPress={() => console.log("register pressed")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
				<List.Item
					title={languages[2][currentLang]}
					left={(props) => <List.Icon {...props} icon={"login"} />}
					right={(props) => (
						<List.Icon {...props} icon="chevron-right" />
					)}
					onPress={() => console.log("login pressed")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
				<List.Item
					title={languages[3][currentLang]}
					left={(props) => <List.Icon {...props} icon={"flag"} />}
					right={(props) => (
						<List.Icon {...props} icon="chevron-right" />
					)}
					onPress={() => props.navigation.navigate("Languages")}
					style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}
				/>
			</ScrollView>
		</View>
	);
};

SettingsScreen.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func,
	}),
};

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
	},
});

export default SettingsScreen;
