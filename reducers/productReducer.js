import {
    GET_HOLIDAYS
} from "../actions/productActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    getHolidaysRes:[],
};

const productReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_HOLIDAYS                                                                                                                               */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_HOLIDAYS.REQUESTED:
            return { ...state };
        case GET_HOLIDAYS.SUCCESS:
            return {
                ...state,
                getHolidaysRes: payload.response,
                errorMsg: '',
            };
        case GET_HOLIDAYS.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        default:
            return state
    }
}

export default productReducer