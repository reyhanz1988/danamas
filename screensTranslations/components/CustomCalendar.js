module.exports =  [
/*0*/
{
    de: "Kalender zurücksetzen",
    en: "Reset Calendar",
    es: "Restablecer calendario",
    fr: "Réinitialiser le calendrier",
    it: "Ripristina calendario",
    nl: "Kalender resetten",
    no: "Tilbakestill kalender",
    pt: "Redefinir calendário",

},

/*1*/
{
    de: "Januar",
    en: "January",
    es: "enero",
    fr: "Janvier",
    it: "Gennaio",
    nl: "Januari",
    no: "Januar",
    pt: "Janeiro",

},

/*2*/
{
    de: "Februar",
    en: "February",
    es: "Febrero",
    fr: "Février",
    it: "Febbraio",
    nl: "Februari",
    no: "Februar",
    pt: "Fevereiro",

},

/*3*/
{
    de: "Marsch",
    en: "March",
    es: "Marcha",
    fr: "Mars",
    it: "Marzo",
    nl: "Maart",
    no: "Mars",
    pt: "Marchar",

},

/*4*/
{
    de: "April",
    en: "April",
    es: "Abril",
    fr: "Avril",
    it: "Aprile",
    nl: "April",
    no: "April",
    pt: "Abril",

},

/*5*/
{
    de: "Mai",
    en: "May",
    es: "Mayo",
    fr: "Mai",
    it: "Maggio",
    nl: "Mei",
    no: "Mai",
    pt: "Maio",

},

/*6*/
{
    de: "Juni",
    en: "June",
    es: "Junio",
    fr: "Juin",
    it: "Giugno",
    nl: "Juni-",
    no: "Juni",
    pt: "Junho",

},

/*7*/
{
    de: "Juli",
    en: "July",
    es: "Mes de julio",
    fr: "Juillet",
    it: "Luglio",
    nl: "Juli-",
    no: "Juli",
    pt: "Julho",

},

/*8*/
{
    de: "August",
    en: "August",
    es: "Agosto",
    fr: "Août",
    it: "Agosto",
    nl: "Augustus",
    no: "August",
    pt: "Agosto",

},

/*9*/
{
    de: "September",
    en: "September",
    es: "Septiembre",
    fr: "Septembre",
    it: "Settembre",
    nl: "September",
    no: "September",
    pt: "Setembro",

},

/*10*/
{
    de: "Oktober",
    en: "October",
    es: "Octubre",
    fr: "Octobre",
    it: "Ottobre",
    nl: "Oktober",
    no: "Oktober",
    pt: "Outubro",

},

/*11*/
{
    de: "November",
    en: "November",
    es: "Noviembre",
    fr: "Novembre",
    it: "Novembre",
    nl: "November",
    no: "November",
    pt: "Novembro",

},

/*12*/
{
    de: "Dezember",
    en: "December",
    es: "Diciembre",
    fr: "Décembre",
    it: "Dicembre",
    nl: "December",
    no: "Desember",
    pt: "Dezembro",

},
];