module.exports =  [
/*0*/
{
    de: "Settings",
    en: "Settings",
    es: "Settings",
    fr: "Settings",
    it: "Settings",
    nl: "Settings",
    no: "Settings",
    pt: "Settings",

},

/*1*/
{
    de: "Registrieren",
    en: "Register",
    es: "Registrarse",
    fr: "S'inscrire",
    it: "Registrati",
    nl: "Register",
    no: "Registrere",
    pt: "Registro",

},

/*2*/
{
    de: "Anmelden",
    en: "Login",
    es: "Login",
    fr: "Connexion",
    it: "Accesso",
    nl: "Log in",
    no: "Logg Inn",
    pt: "Login",

},

/*3*/
{
    de: "Sprachen",
    en: "Languages",
    es: "Idiomas",
    fr: "Langages",
    it: "Lingua",
    nl: "Talen",
    no: "Språk",
    pt: "Línguas",

},
];