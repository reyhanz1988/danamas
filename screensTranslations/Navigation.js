module.exports =  [
/*0*/
{
    de: "Heim",
    en: "Home",
    es: "Casa",
    fr: "Accueil",
    it: "Casa",
    nl: "Huis",
    no: "Hjem",
    pt: "Casa",

},

/*1*/
{
    de: "Einstellungen",
    en: "Settings",
    es: "Ajustes",
    fr: "Réglages",
    it: "Impostazioni",
    nl: "Instellingen",
    no: "Innstillinger",
    pt: "Configurações",

},
];