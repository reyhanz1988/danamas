import { API_URL } from "@env";

let ProductApi = {
	getHolidays(vars) {
		let params = {
			method: "GET",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
		};
		return fetch(
			API_URL +
				"/api?month=" +
				vars.currentMonth +
				"&year=" +
				vars.currentYear,
			params
		)
			.then((res) => res.json())
			.catch((error) => {
				console.warn("getHolidays => " + error);
				return "error";
			});
	},
};

module.exports = ProductApi;
