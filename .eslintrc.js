module.exports = {
	env: {
		es2021: true,
		node: true,
		"jest/globals": true,
	},
	extends: [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:jest/recommended",
		"plugin:react/jsx-runtime",
	],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: "latest",
		sourceType: "module",
	},
	globals: {
		fetch: false,
	},
	plugins: ["react"],
	rules: {
		"react/jsx-uses-react": "error",
		"react/jsx-uses-vars": "error",
	},
	settings: {
		react: {
			version: "detect",
		},
	},
};
