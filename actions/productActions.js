import ProductApi from '../api/ProductApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';

// getHolidays
export const GET_HOLIDAYS = createAsyncActionType('GET_HOLIDAYS');
export const getHolidays = vars => dispatch => {
    dispatch({
        type: GET_HOLIDAYS.REQUESTED,
    });
    ProductApi.getHolidays(vars)
    .then(response => {
        dispatch({
            type: GET_HOLIDAYS.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_HOLIDAYS.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};