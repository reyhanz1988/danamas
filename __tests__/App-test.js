//npm run test -- -i -u
import React from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { create, act } from "react-test-renderer";
import HomeScreen from "../screens/HomeScreen";
import LanguagesScreen from "../screens/LanguagesScreen";
import SettingsScreen from "../screens/SettingsScreen";
import { Provider } from "react-redux";
import store from "../store";
import rootReducer from "../reducers/rootReducer";
import productReducer from "../reducers/productReducer";
import masterReducer from "../reducers/masterReducer";

jest.spyOn(console, "warn").mockImplementation(() => {});
jest.spyOn(console, "error").mockImplementation(() => {});

fetch = jest.fn(() => Promise.resolve());

it("checks if Async Storage is used", async () => {
	await AsyncStorage.setItem("LANG", "en");
	expect(AsyncStorage.getItem).toBeCalledWith("LANG");
});

const tree = create(
	<Provider store={store}>
		<HomeScreen />
		<LanguagesScreen />
		<SettingsScreen />
	</Provider>
);

describe("AllScreen", () => {
	it("AllScreen should render properly", () => {
		act(() => {
			expect(tree).toMatchSnapshot();
		});
	});
});

describe("productReducer", () => {
	it("productReducer => initial state", () => {
		expect(rootReducer(productReducer, {})).toMatchSnapshot();
	});
	it("productReducer => GET_HOLIDAYS", () => {
		act(() => {
			expect(
				rootReducer(productReducer, { type: "GET_HOLIDAYS" })
			).toMatchSnapshot();
		});
	});
});

describe("masterReducer", () => {
	it("masterReducer => initial state", () => {
		expect(rootReducer(masterReducer, {})).toMatchSnapshot();
	});
	it("masterReducer => GET_LANG", () => {
		act(() => {
			expect(
				rootReducer(masterReducer, { type: "GET_LANG" })
			).toMatchSnapshot();
		});
	});
});
